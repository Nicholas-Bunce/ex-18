﻿using System;

namespace ex_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var numa1 = 5;
            var numa2 = 8;
            var numa3 = 6;
            var numa4 = 7;
            var numa5 = 3;

            Console.WriteLine($"the value of numa1 = {numa1} ");
            Console.WriteLine($"the value of numa2 = {numa2} ");
            Console.WriteLine($"the value of numa3 = {numa3} ");
            Console.WriteLine($"the value of numa4 = {numa4} ");
            Console.WriteLine($"the value of numa5 = {numa5} ");

            Console.WriteLine("===============================");
            var numb1 = 2;
            var numb2 = 6;
            var numb3 = 8;
            var numb4 = 9;
            var numb5 = 0;
            var numb6 = 4;

            Console.WriteLine($"the value of numa1 = {numb1} ");
            Console.WriteLine($"the value of numa2 = {numb2} ");
            Console.WriteLine($"the value of numa3 = {numb3} ");
            Console.WriteLine($"the value of numa4 = {numb4} ");
            Console.WriteLine($"the value of numa5 = {numb5} ");
            Console.WriteLine($"the value of numa5 = {numb6} ");

            Console.WriteLine($" numb1 + numb6 = {numb1 + numb6} ");
            Console.WriteLine($" numb2 + numb5 = {numb2 + numb5} ");
            Console.WriteLine($" numb3 + numb4 = {numb3 + numb4} ");

            Console.WriteLine("===============================");
            var numc1 = 16;
            var numc2 = 30;
            var numc3 = 55;
            var numc4 = 2;

            Console.WriteLine($"numc1 + numc2 = {numc1 += numc2} ");
            Console.WriteLine($"numc3 + numc4 = {numc3 += numc4} ");
            Console.WriteLine($"numc3 + numc2 = {numc3 += numc2} ");
            Console.WriteLine($"numc4 + numc2 = {numc4 += numc2} ");

            Console.WriteLine("===============================");
            


        }
    }
}
